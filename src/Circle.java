public class Circle extends Shape {
    double radius = 1.0;

    public Circle() {
        return;
    }
    public Circle(double radius) {
        this.radius = radius;
    }
    public Circle(double radius, String color, boolean filled) {
        this.color = color;
        this.filled = filled;
        this.radius = radius;
    }
    public double getRadius() {
        return radius;
    }
    public void setRadius(double radius) {
        this.radius = radius;
    }
    public double getArea() {
        return radius * radius * Math.PI;
    }
    public double getPerimeter() {
        return (radius * 2) * Math.PI;
    }
    @Override
    public String toString() {
        return "Circle[Shape[color=" + getColor() + ",filled=" + isFilled() + "],radius=" + getRadius() + "]";
    }
}
