public class Main {
    public static void main(String[] args) {
        System.out.println("Hello world!");

        Shape shape1 = new Shape();
        Shape shape2 = new Shape("green", false);
        System.out.println(shape1 + "," + shape2);

        Circle circle1 = new Circle();
        Circle circle2 = new Circle(2.0);
        Circle circle3 = new Circle(3.0, "green", true);
        System.out.println(circle1 + "," + circle2 + "," + circle3);

        Rectangle rectangle1 = new Rectangle();
        Rectangle rectangle2 = new Rectangle(2.5, 1.5);
        Rectangle rectangle3 = new Rectangle(2.0, 1.5, "green", true);
        System.out.println(rectangle1 + "," + rectangle2 + "," + rectangle3);

        Square square1 = new Square();
        Square square2 = new Square(1.5);
        Square square3 = new Square(2.0, "green", true);
        System.out.println(square1 + "," + square2 + "," + square3);

        System.out.println("Area circle1=" + circle1.getArea());
        System.out.println("Area circle2=" + circle2.getArea());
        System.out.println("Area circle3=" + circle3.getArea());
        System.out.println("Perimeter circle1=" + circle1.getPerimeter());
        System.out.println("Perimeter circle2=" + circle2.getPerimeter());
        System.out.println("Perimeter circle3=" + circle3.getPerimeter());

        System.out.println("Area rectangle1=" + rectangle1.getArea());
        System.out.println("Area rectangle2=" + rectangle2.getArea());
        System.out.println("Area rectangle3=" + rectangle3.getArea());
        System.out.println("Perimeter rectangle1=" + rectangle1.getPerimeter());
        System.out.println("Perimeter rectangle2=" + rectangle2.getPerimeter());
        System.out.println("Perimeter rectangle3=" + rectangle3.getPerimeter());

        System.out.println("Area square1=" + square1.getArea());
        System.out.println("Area square2=" + square2.getArea());
        System.out.println("Area square3=" + square3.getArea());
        System.out.println("Perimeter square1=" + square1.getPerimeter());
        System.out.println("Perimeter square2=" + square2.getPerimeter());
        System.out.println("Perimeter square3=" + square3.getPerimeter());
    }
}